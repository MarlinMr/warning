import praw
from datetime import datetime
import time
from pprint import pprint
import winsound
from collections import Counter
import sys
sys.path.append('F:\credentials\\')
#sys.path.append('/home/pi/credentials')
import config as cfg
reddit = praw.Reddit(client_id=cfg.client_id,
                     client_secret=cfg.client_secret,
                     user_agent=cfg.user_agent,
                     username=cfg.username,
                     password=cfg.password)
f1 = 880
f2 = 1760
duration = 200 
wordList = []
names = ["ROSENSTEIN","MULLER"]
badWordList = ["RESIGNS","FIRE","OUST"]
subs = 'politics+news+worldnews'
filterWords = ["WILL","WHO","IF","NOT","HE","THAT","ARE","HIMSELF","BE","TO", "AND", "OF", "THE","A","IT","FOR","HIS","EVIDENCE","REPORT","IN","ON","IS","AT","AFTER","AS","HAS","SAYS","WITH","OFF","BY","WAS","ABOUT","WHAT","NEW","THIS"]
counter=Counter()

def alert():
	winsound.Beep(f1, duration)
	winsound.Beep(f2, duration)

def wordsInText(title,words):
    return ((len(words) > 0 and len(title.split()) > 0) and ((words[0] in title.split()[0]) or wordsInText(title,words[1:]) or wordsInText(" ".join(title.split()[1:]),words)))
	
for submission in reddit.subreddit(subs).stream.submissions():
	try:
		datoTid = datetime.fromtimestamp(submission.created_utc)
		wordList = (str(submission.title).upper().split()+wordList)[:2000]
		wordList = [item for item in wordList if item not in filterWords]
		counter=Counter(wordList)
		title = submission.title.upper()
		if(wordsInText(title,badWordList) and wordsInText(title,names)):
			alert()
		print("{}{}{}\n{}\n".format(
			str(submission.subreddit).ljust(10,' '),
			str(datoTid.strftime("%d/%m/%Y %H:%M:%S ")),
			str(counter.most_common(10)),
			submission.title))
	except Exception as e:
		pass
		
